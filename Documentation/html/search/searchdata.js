var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuw",
  1: "acefginrtu",
  2: "cdegntuw",
  3: "acdefginprstu",
  4: "_abcdefgiklmoprstw",
  5: "iu",
  6: "i",
  7: "abcdefimnoprstuw",
  8: "_abcdefghimnoprstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

