var searchData=
[
  ['afio',['AFIO',['../common_8h.html#a582e09473429414015b1de90cf767fa8',1,'common.h']]],
  ['afio_5fbase',['AFIO_BASE',['../common_8h.html#a5f7e3eacfcf4c313c25012795148a680',1,'common.h']]],
  ['afio_5fclocken',['AFIO_CLOCKEN',['../gpio_8h.html#a5da24619ba0ee1b35a01cf7ee2433b3a',1,'gpio.h']]],
  ['ahbperiph_5fbase',['AHBPERIPH_BASE',['../common_8h.html#a92eb5d49730765d2abd0f5b09548f9f5',1,'common.h']]],
  ['apb1periph_5fbase',['APB1PERIPH_BASE',['../common_8h.html#a45666d911f39addd4c8c0a0ac3388cfb',1,'common.h']]],
  ['apb2periph_5fbase',['APB2PERIPH_BASE',['../common_8h.html#a25b99d6065f1c8f751e78f43ade652cb',1,'common.h']]],
  ['app1bclk',['APP1BCLK',['../clk_8h.html#af8906847b65c19c5f41328df629389dd',1,'clk.h']]]
];
