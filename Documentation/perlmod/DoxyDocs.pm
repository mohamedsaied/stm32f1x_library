$doxydocs=
{
  classes => [
    {
      name => 'AFIO_TypeDef',
      includes => {
        local => 'no',
        name => 'common.h'
      },
      all_members => [
        {
          name => 'EVCR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'AFIO_TypeDef'
        },
        {
          name => 'EXTICR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'AFIO_TypeDef'
        },
        {
          name => 'MAPR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'AFIO_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'EVCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'MAPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'EXTICR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t',
            arguments => '[4]'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'CAN_FIFOMailBox_TypeDef',
      includes => {
        local => 'no',
        name => 'can.h'
      },
      all_members => [
        {
          name => 'RDHR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FIFOMailBox_TypeDef'
        },
        {
          name => 'RDLR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FIFOMailBox_TypeDef'
        },
        {
          name => 'RDTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FIFOMailBox_TypeDef'
        },
        {
          name => 'RIR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FIFOMailBox_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'RIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RDTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RDLR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RDHR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'CAN_FilterRegister_TypeDef',
      includes => {
        local => 'no',
        name => 'can.h'
      },
      all_members => [
        {
          name => 'FR1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FilterRegister_TypeDef'
        },
        {
          name => 'FR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_FilterRegister_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'FR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'FR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'CAN_msg',
      includes => {
        local => 'no',
        name => 'can.h'
      },
      all_members => [
        {
          name => 'data',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_msg'
        },
        {
          name => 'format',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_msg'
        },
        {
          name => 'id',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_msg'
        },
        {
          name => 'len',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_msg'
        },
        {
          name => 'type',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_msg'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'id',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned int'
          },
          {
            kind => 'variable',
            name => 'data',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'len',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char'
          },
          {
            kind => 'variable',
            name => 'format',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char'
          },
          {
            kind => 'variable',
            name => 'type',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'CAN_TxMailBox_TypeDef',
      includes => {
        local => 'no',
        name => 'can.h'
      },
      all_members => [
        {
          name => 'TDHR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TxMailBox_TypeDef'
        },
        {
          name => 'TDLR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TxMailBox_TypeDef'
        },
        {
          name => 'TDTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TxMailBox_TypeDef'
        },
        {
          name => 'TIR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TxMailBox_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'TIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'TDTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'TDLR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'TDHR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'CAN_TypeDef',
      includes => {
        local => 'no',
        name => 'can.h'
      },
      all_members => [
        {
          name => 'BTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'ESR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'FA1R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'FFA1R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'FM1R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'FMR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'FS1R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'IER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'MCR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'MSR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED0',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED4',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RESERVED5',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RF0R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'RF1R',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'sFIFOMailBox',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'sFilterRegister',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'sTxMailBox',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        },
        {
          name => 'TSR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'CAN_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'MCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'MSR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'TSR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RF0R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RF1R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'IER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'ESR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'BTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[88]'
          },
          {
            kind => 'variable',
            name => 'sTxMailBox',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_TxMailBox_TypeDef',
            arguments => '[3]'
          },
          {
            kind => 'variable',
            name => 'sFIFOMailBox',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_FIFOMailBox_TypeDef',
            arguments => '[2]'
          },
          {
            kind => 'variable',
            name => 'RESERVED1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[12]'
          },
          {
            kind => 'variable',
            name => 'FMR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'FM1R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t'
          },
          {
            kind => 'variable',
            name => 'FS1R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t'
          },
          {
            kind => 'variable',
            name => 'FFA1R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t'
          },
          {
            kind => 'variable',
            name => 'FA1R',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'sFilterRegister',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_FilterRegister_TypeDef',
            arguments => '[28]'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'EXTI_TypeDef',
      includes => {
        local => 'no',
        name => 'extint.h'
      },
      all_members => [
        {
          name => 'EMR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        },
        {
          name => 'FTSR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        },
        {
          name => 'IMR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        },
        {
          name => 'PR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        },
        {
          name => 'RTSR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        },
        {
          name => 'SWIER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'EXTI_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'IMR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'EMR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RTSR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'FTSR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'SWIER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'PR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'FLASH_TypeDef',
      includes => {
        local => 'no',
        name => 'common.h'
      },
      all_members => [
        {
          name => 'ACR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'AR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'CR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'KEYR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'OBR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'OPTKEYR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'RESERVED',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'SR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        },
        {
          name => 'WRPR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'FLASH_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'ACR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'KEYR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'OPTKEYR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'SR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'AR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'OBR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'WRPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'GPIO_TypeDef',
      includes => {
        local => 'no',
        name => 'gpio.h'
      },
      all_members => [
        {
          name => 'BRR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'BSRR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'CRH',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'CRL',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'IDR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'LCKR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        },
        {
          name => 'ODR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'GPIO_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'CRL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CRH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'IDR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'ODR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'IWDG_TypeDef',
      includes => {
        local => 'no',
        name => 'watchdog.h'
      },
      all_members => [
        {
          name => 'KR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'IWDG_TypeDef'
        },
        {
          name => 'PR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'IWDG_TypeDef'
        },
        {
          name => 'RLR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'IWDG_TypeDef'
        },
        {
          name => 'SR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'IWDG_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'KR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'PR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'RLR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'SR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'NVIC_Type',
      includes => {
        local => 'no',
        name => 'common.h'
      },
      all_members => [
        {
          name => 'IABR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'ICER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'ICPR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'IP',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'ISER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'ISPR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RESERVED0',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RESERVED2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RESERVED3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RESERVED4',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RESERVED5',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'RSERVED1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        },
        {
          name => 'STIR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'NVIC_Type'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'ISER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x000 (R/W) Interrupt Set Enable Register '
                }
              ]
            },
            type => '__IO uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'RESERVED0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[24]'
          },
          {
            kind => 'variable',
            name => 'ICER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x080 (R/W) Interrupt Clear Enable Register '
                }
              ]
            },
            type => '__IO uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'RSERVED1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[24]'
          },
          {
            kind => 'variable',
            name => 'ISPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x100 (R/W) Interrupt Set Pending Register '
                }
              ]
            },
            type => '__IO uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'RESERVED2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[24]'
          },
          {
            kind => 'variable',
            name => 'ICPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x180 (R/W) Interrupt Clear Pending Register '
                }
              ]
            },
            type => '__IO uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'RESERVED3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[24]'
          },
          {
            kind => 'variable',
            name => 'IABR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x200 (R/W) Interrupt Active bit Register '
                }
              ]
            },
            type => '__IO uint32_t',
            arguments => '[8]'
          },
          {
            kind => 'variable',
            name => 'RESERVED4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[56]'
          },
          {
            kind => 'variable',
            name => 'IP',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0x300 (R/W) Interrupt Priority Register (8Bit wide) '
                }
              ]
            },
            type => '__IO uint8_t',
            arguments => '[240]'
          },
          {
            kind => 'variable',
            name => 'RESERVED5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t',
            arguments => '[644]'
          },
          {
            kind => 'variable',
            name => 'STIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Offset: 0xE00 ( /W) Software Trigger Interrupt Register '
                }
              ]
            },
            type => '__O uint32_t'
          }
        ]
      },
      brief => {
        doc => [
          {
            type => 'parbreak'
          },
          {
            type => 'text',
            content => 'Structure type to access the Nested Vectored Interrupt Controller (NVIC). '
          }
        ]
      },
      detailed => {}
    },
    {
      name => 'RCC_TypeDef',
      includes => {
        local => 'no',
        name => 'common.h'
      },
      all_members => [
        {
          name => 'AHBENR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'AHBSTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'APB1ENR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'APB1RSTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'APB2ENR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'APB2RSTR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'BDCR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'CFGR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'CFGR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'CIR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'CR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        },
        {
          name => 'CSR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'RCC_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'CR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CFGR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'APB2RSTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'APB1RSTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'AHBENR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'APB2ENR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'APB1ENR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'BDCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CSR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'AHBSTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          },
          {
            kind => 'variable',
            name => 'CFGR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint32_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'TIM_GP_TypeDef',
      includes => {
        local => 'no',
        name => 'timer.h'
      },
      all_members => [
        {
          name => 'ARR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCMR1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCMR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCR1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCR3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CCR4',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CNT',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CR1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'CR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'DCR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'DIER',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'DMAR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'EGR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'PSC',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED0',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED10',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED11',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED12',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED13',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED14',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED15',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED16',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED17',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED4',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED5',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED6',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED7',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED8',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'RESERVED9',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'SMCR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        },
        {
          name => 'SR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'TIM_GP_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'CR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'SMCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'DIER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'SR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'EGR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCMR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCMR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED8',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CNT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED9',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'PSC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED10',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'ARR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED11',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t',
            arguments => '[3]'
          },
          {
            kind => 'variable',
            name => 'CCR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED12',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED13',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED14',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CCR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED15',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t',
            arguments => '[3]'
          },
          {
            kind => 'variable',
            name => 'DCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED16',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'DMAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED17',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'USART_TypeDef',
      includes => {
        local => 'no',
        name => 'usart.h'
      },
      all_members => [
        {
          name => 'BRR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'CR1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'CR2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'CR3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'DR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'GTPR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED0',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED1',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED2',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED3',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED4',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED5',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'RESERVED6',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        },
        {
          name => 'SR',
          virtualness => 'non_virtual',
          protection => 'public',
          scope => 'USART_TypeDef'
        }
      ],
      public_members => {
        members => [
          {
            kind => 'variable',
            name => 'SR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'DR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'CR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'variable',
            name => 'GTPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => '__IO uint16_t'
          },
          {
            kind => 'variable',
            name => 'RESERVED6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          }
        ]
      },
      brief => {},
      detailed => {}
    }
  ],
  namespaces => [
  ],
  files => [
    {
      name => 'can.c',
      includes => [
        {
          name => 'can.h',
          ref => 'can_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'canInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => '_mode',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'CAN_wrMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'msg',
                type => 'CAN_msg *'
              },
              {
                declaration_name => 'mailIndex',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'canTransmit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'id',
                type => 'int'
              },
              {
                declaration_name => 'idtype',
                type => 'char'
              },
              {
                declaration_name => 'ftype',
                type => 'char'
              },
              {
                declaration_name => 'm',
                type => 'unsigned char *'
              },
              {
                declaration_name => '_mailIndex',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'CAN_wrFilter',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'id',
                type => 'unsigned int'
              },
              {
                declaration_name => 'format',
                type => 'unsigned char'
              },
              {
                declaration_name => 'mess_type',
                type => 'unsigned char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'filtersInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => '_id',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'canRead',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'msg',
                type => 'CAN_msg *'
              },
              {
                declaration_name => 'fifoIndex',
                type => 'int'
              }
            ]
          }
        ]
      },
      variables => {
        members => [
          {
            kind => 'variable',
            name => 'CAN_TxMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_msg'
          },
          {
            kind => 'variable',
            name => 'CAN_RxMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_msg'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'can.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        },
        {
          name => 'gpio.h',
          ref => 'gpio_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/can.c',
          ref => 'can_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'STANDARD_FORMAT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'EXTENDED_FORMAT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'POLLING',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '!(1<<1)'
          },
          {
            kind => 'define',
            name => 'INTERRUPT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1<<1)'
          },
          {
            kind => 'define',
            name => 'DATA_FRAME',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'REMOTE_FRAME',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'MAILBOX0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'MAILBOX1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'MAILBOX2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'FIFO0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'FIFO1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'CAN1_ReceiveFIFO',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'CAN1->RF0R'
          },
          {
            kind => 'define',
            name => 'CAN2_ReceiveFIFO',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'CAN2->RF0R'
          },
          {
            kind => 'define',
            name => 'RELEASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1<<5)'
          },
          {
            kind => 'define',
            name => 'CAN1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((CAN_TypeDef    *) CAN1_BASE    )'
          },
          {
            kind => 'define',
            name => 'CAN2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((CAN_TypeDef    *) CAN2_BASE    )'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'canInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => '_mode',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'CAN_wrMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'msg',
                type => 'CAN_msg *'
              },
              {
                declaration_name => 'mailIndex',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'canTransmit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'id',
                type => 'int'
              },
              {
                declaration_name => 'idtype',
                type => 'char'
              },
              {
                declaration_name => 'ftype',
                type => 'char'
              },
              {
                declaration_name => 'm',
                type => 'unsigned char *'
              },
              {
                declaration_name => '_mailIndex',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'CAN_wrFilter',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'id',
                type => 'unsigned int'
              },
              {
                declaration_name => 'format',
                type => 'unsigned char'
              },
              {
                declaration_name => 'mess_type',
                type => 'unsigned char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'filtersInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => '_id',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'canRead',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'CAN',
                type => 'CAN_TypeDef *'
              },
              {
                declaration_name => 'msg',
                type => 'CAN_msg *'
              },
              {
                declaration_name => 'fifoIndex',
                type => 'int'
              }
            ]
          }
        ]
      },
      variables => {
        members => [
          {
            kind => 'variable',
            name => 'CAN_TxMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_msg'
          },
          {
            kind => 'variable',
            name => 'CAN_RxMsg',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'CAN_msg'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'clk.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'HSE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '16'
          },
          {
            kind => 'define',
            name => 'HSI',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'HSEON',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '17'
          },
          {
            kind => 'define',
            name => 'HSION',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'PLL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '24'
          },
          {
            kind => 'define',
            name => 'PLLLOCKED',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '25'
          },
          {
            kind => 'define',
            name => 'PLLMUL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '18'
          },
          {
            kind => 'define',
            name => 'APP1BCLK',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '10'
          },
          {
            kind => 'define',
            name => 'PLLSCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '16'
          },
          {
            kind => 'define',
            name => 'PSC1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {}
          },
          {
            kind => 'define',
            name => 'CLKUPTO48',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x11'
          },
          {
            kind => 'define',
            name => 'CLKUPTO72',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x12'
          },
          {
            kind => 'define',
            name => 'PSC1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {}
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'initClk',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                type => 'void'
              }
            ]
          },
          {
            kind => 'function',
            name => 'delayus',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => '__t',
                type => 'unsigned long'
              }
            ]
          },
          {
            kind => 'function',
            name => 'delayms',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => '__t',
                type => 'unsigned long'
              }
            ]
          }
        ]
      },
      variables => {
        members => [
          {
            kind => 'variable',
            name => '__clk',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'int',
            initializer => '=8'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'common.h',
      includes => [
        {
          name => 'stdint.h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/can.h',
          ref => 'can_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/gpio.h',
          ref => 'gpio_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/clk.h',
          ref => 'clk_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/dma.h',
          ref => 'dma_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/extint.h',
          ref => 'extint_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/nvic.h',
          ref => 'nvic_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/timer.h',
          ref => 'timer_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/usart.h',
          ref => 'usart_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/watchdog.h',
          ref => 'watchdog_8h'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => '__O',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Defines \'write only\' permissions '
                }
              ]
            },
            initializer => 'volatile const'
          },
          {
            kind => 'define',
            name => '__IO',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Defines \'read / write\' permissions '
                }
              ]
            },
            initializer => 'volatile'
          },
          {
            kind => 'define',
            name => 'SRAM_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((     uint32_t)0x20000000)'
          },
          {
            kind => 'define',
            name => 'PERIPH_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((     uint32_t)0x40000000)'
          },
          {
            kind => 'define',
            name => 'TRUE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'FALSE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'OB_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((     uint32_t)0x1FFFF800)'
          },
          {
            kind => 'define',
            name => 'INFO_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((uint32_t)0x1ffff000)'
          },
          {
            kind => 'define',
            name => 'APB1PERIPH_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'PERIPH_BASE'
          },
          {
            kind => 'define',
            name => 'APB2PERIPH_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PERIPH_BASE + 0x10000)'
          },
          {
            kind => 'define',
            name => 'AHBPERIPH_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PERIPH_BASE + 0x20000)'
          },
          {
            kind => 'define',
            name => 'AFIO_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x0000)'
          },
          {
            kind => 'define',
            name => 'GPIOA_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x0800)'
          },
          {
            kind => 'define',
            name => 'GPIOB_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x0C00)'
          },
          {
            kind => 'define',
            name => 'GPIOC_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x1000)'
          },
          {
            kind => 'define',
            name => 'GPIOD_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x1400)'
          },
          {
            kind => 'define',
            name => 'GPIOE_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x1800)'
          },
          {
            kind => 'define',
            name => 'WWDG_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x2C00)'
          },
          {
            kind => 'define',
            name => 'IWDG_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x3000)'
          },
          {
            kind => 'define',
            name => 'CAN1_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x6400)'
          },
          {
            kind => 'define',
            name => 'CAN2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x6800)'
          },
          {
            kind => 'define',
            name => 'EXTI_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x0400)'
          },
          {
            kind => 'define',
            name => 'TIM2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x0000)'
          },
          {
            kind => 'define',
            name => 'TIM3_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x0400)'
          },
          {
            kind => 'define',
            name => 'TIM4_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x0800)'
          },
          {
            kind => 'define',
            name => 'TIM5_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x0C00)'
          },
          {
            kind => 'define',
            name => 'USART2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4400)'
          },
          {
            kind => 'define',
            name => 'USART3_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4800)'
          },
          {
            kind => 'define',
            name => 'USART4_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4C00)'
          },
          {
            kind => 'define',
            name => 'USART5_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x5000)'
          },
          {
            kind => 'define',
            name => 'CAN1_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x6400)'
          },
          {
            kind => 'define',
            name => 'CAN2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x6800)'
          },
          {
            kind => 'define',
            name => 'RCC_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(AHBPERIPH_BASE  + 0x1000)'
          },
          {
            kind => 'define',
            name => 'FLASH_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(AHBPERIPH_BASE  + 0x2000)'
          },
          {
            kind => 'define',
            name => 'FLASH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((FLASH_TypeDef  *) FLASH_BASE   )'
          },
          {
            kind => 'define',
            name => 'RCC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((RCC_TypeDef    *) RCC_BASE     )'
          },
          {
            kind => 'define',
            name => 'AFIO',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((AFIO_TypeDef   *) AFIO_BASE    )'
          },
          {
            kind => 'define',
            name => 'NVIC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'NVIC configuration struct '
                }
              ]
            },
            initializer => '((NVIC_Type      *)     NVIC_BASE     )'
          },
          {
            kind => 'define',
            name => 'MMIO8',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'addr'
              }
            ],
            initializer => '(*(volatile u8 *)(addr))'
          },
          {
            kind => 'define',
            name => 'MMIO16',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'addr'
              }
            ],
            initializer => '(*(volatile u16 *)(addr))'
          },
          {
            kind => 'define',
            name => 'MMIO32',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'addr'
              }
            ],
            initializer => '(*(volatile u32 *)(addr))'
          },
          {
            kind => 'define',
            name => 'MMIO64',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'addr'
              }
            ],
            initializer => '(*(volatile u64 *)(addr))'
          },
          {
            kind => 'define',
            name => 'PPBI_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0xE0000000'
          },
          {
            kind => 'define',
            name => 'ITM_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PPBI_BASE + 0x0000)'
          },
          {
            kind => 'define',
            name => 'DWT_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PPBI_BASE + 0x1000)'
          },
          {
            kind => 'define',
            name => 'FPB_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PPBI_BASE + 0x2000)'
          },
          {
            kind => 'define',
            name => 'SCS_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PPBI_BASE + 0xE000)'
          },
          {
            kind => 'define',
            name => 'TPIU_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(PPBI_BASE + 0x40000)'
          },
          {
            kind => 'define',
            name => 'ITR_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0000)'
          },
          {
            kind => 'define',
            name => 'SYS_TICK_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0010)'
          },
          {
            kind => 'define',
            name => 'NVIC_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0100)'
          },
          {
            kind => 'define',
            name => 'SCB_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0D00)'
          },
          {
            kind => 'define',
            name => 'STIR_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0F00)'
          },
          {
            kind => 'define',
            name => 'ID_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(SCS_BASE + 0x0FD0)'
          },
          {
            kind => 'define',
            name => 'USART1_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB2PERIPH_BASE + 0x3800)'
          }
        ]
      },
      typedefs => {
        members => [
          {
            kind => 'typedef',
            name => 'u8',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint8_t'
          },
          {
            kind => 'typedef',
            name => 'u16',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint16_t'
          },
          {
            kind => 'typedef',
            name => 'u32',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint32_t'
          },
          {
            kind => 'typedef',
            name => 'u64',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'uint64_t'
          },
          {
            kind => 'typedef',
            name => 'IRQn_Type',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'enum IRQn'
          }
        ]
      },
      enums => {
        members => [
          {
            kind => 'enum',
            name => 'IRQn',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            values => [
              {
                name => 'NonMaskableInt_IRQn',
                initializer => '= -14',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '2 Non Maskable Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'MemoryManagement_IRQn',
                initializer => '= -12',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'text',
                      content => '4 Cortex-M3 Memory Management Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'BusFault_IRQn',
                initializer => '= -11',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '5 Cortex-M3 Bus Fault Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'UsageFault_IRQn',
                initializer => '= -10',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '6 Cortex-M3 Usage Fault Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'SVCall_IRQn',
                initializer => '= -5',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '11 Cortex-M3 SV Call Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DebugMonitor_IRQn',
                initializer => '= -4',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '12 Cortex-M3 Debug Monitor Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'PendSV_IRQn',
                initializer => '= -2',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'text',
                      content => '14 Cortex-M3 Pend SV Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'SysTick_IRQn',
                initializer => '= -1',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => '15 Cortex-M3 System Tick Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'WWDG_IRQn',
                initializer => '= 0',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'Window WatchDog Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'PVD_IRQn',
                initializer => '= 1',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'PVD through EXTI Line detection Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TAMPER_IRQn',
                initializer => '= 2',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'Tamper Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'RTC_IRQn',
                initializer => '= 3',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'RTC global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'FLASH_IRQn',
                initializer => '= 4',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'text',
                      content => 'FLASH global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'RCC_IRQn',
                initializer => '= 5',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'text',
                      content => 'RCC global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI0_IRQn',
                initializer => '= 6',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'EXTI Line0 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI1_IRQn',
                initializer => '= 7',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'EXTI Line1 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI2_IRQn',
                initializer => '= 8',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'EXTI Line2 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI3_IRQn',
                initializer => '= 9',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'EXTI Line3 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI4_IRQn',
                initializer => '= 10',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'EXTI Line4 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel1_IRQn',
                initializer => '= 11',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 1 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel2_IRQn',
                initializer => '= 12',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 2 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel3_IRQn',
                initializer => '= 13',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 3 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel4_IRQn',
                initializer => '= 14',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 4 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel5_IRQn',
                initializer => '= 15',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 5 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel6_IRQn',
                initializer => '= 16',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'text',
                      content => 'DMA1 Channel 6 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA1_Channel7_IRQn',
                initializer => '= 17',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA1 Channel 7 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'ADC_IRQn',
                initializer => '= 18',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'ADC global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'CAN1_TX_IRQn',
                initializer => '= 19',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN1 TX Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'CAN1_RX0_IRQn',
                initializer => '= 20',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN1 RX0 Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'CAN1_RX1_IRQn',
                initializer => '= 21',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN1 RX1 Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'CAN1_SCE_IRQn',
                initializer => '= 22',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN1 SCE Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI9_5_IRQn',
                initializer => '= 23',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'External Line[9:5] Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'TIM1_BRK_IRQn',
                initializer => '= 24',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM1 Break Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM1_UP_IRQn',
                initializer => '= 25',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM1 Update Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM1_TRG_COM_IRQn',
                initializer => '= 26',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM1 Trigger and Commutation Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM1_CC_IRQn',
                initializer => '= 27',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM1 Capture Compare Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM2_IRQn',
                initializer => '= 28',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM2 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM3_IRQn',
                initializer => '= 29',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM3 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM4_IRQn',
                initializer => '= 30',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM4 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'I2C1_EV_IRQn',
                initializer => '= 31',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'I2C1 Event Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'I2C1_ER_IRQn',
                initializer => '= 32',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'I2C1 Error Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'I2C2_EV_IRQn',
                initializer => '= 33',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'I2C2 Event Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'I2C2_ER_IRQn',
                initializer => '= 34',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'I2C2 Error Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'SPI1_IRQn',
                initializer => '= 35',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'SPI1 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'SPI2_IRQn',
                initializer => '= 36',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'SPI2 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'USART1_IRQn',
                initializer => '= 37',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'USART1 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'USART2_IRQn',
                initializer => '= 38',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'USART2 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'USART3_IRQn',
                initializer => '= 39',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'USART3 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'EXTI15_10_IRQn',
                initializer => '= 40',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'External Line[15:10] Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'RTCAlarm_IRQn',
                initializer => '= 41',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'RTC Alarm through EXTI Line Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'OTG_FS_WKUP_IRQn',
                initializer => '= 42',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'USB On-The-Go FS Wakeup through EXTI Line Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM5_IRQn',
                initializer => '= 50',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM5 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'SPI3_IRQn',
                initializer => '= 51',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'SPI3 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'UART4_IRQn',
                initializer => '= 52',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'UART4 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'UART5_IRQn',
                initializer => '= 53',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'UART5 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM6_IRQn',
                initializer => '= 54',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM6 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'TIM7_IRQn',
                initializer => '= 55',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'TIM7 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA2_Channel1_IRQn',
                initializer => '= 56',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA2 Channel1 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA2_Channel2_IRQn',
                initializer => '= 57',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA2 Channel2 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA2_Channel3_IRQn',
                initializer => '= 58',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA2 Channel3 global Interrupt '
                    }
                  ]
                }
              },
              {
                name => 'DMA2_Channel4_IRQn',
                initializer => '= 59',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA2 Channel4 global Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'DMA2_Channel5_IRQn',
                initializer => '= 60',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'DMA2 Channel5 global Interrupts '
                    }
                  ]
                }
              },
              {
                name => 'ETH_IRQn',
                initializer => '= 61',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'Ethernet global interrupt '
                    }
                  ]
                }
              },
              {
                name => 'ETH_WKUP_IRQn',
                initializer => '= 62',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'Ethernet Wakeup through EXTI line interrupt '
                    }
                  ]
                }
              },
              {
                name => 'CAN2_TX_IRQn',
                initializer => '= 63',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN2 TX interrupts '
                    }
                  ]
                }
              },
              {
                name => 'CAN2_RX0_IRQn',
                initializer => '= 64',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN2 RX0 interrupts '
                    }
                  ]
                }
              },
              {
                name => 'CAN2_RX1_IRQn',
                initializer => '= 65',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN2 RX1 interrupt '
                    }
                  ]
                }
              },
              {
                name => 'CAN2_SCE_IRQn',
                initializer => '= 66',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'CAN2 SCE interrupt '
                    }
                  ]
                }
              },
              {
                name => 'OTG_FS_IRQn',
                initializer => '= 67',
                brief => {},
                detailed => {
                  doc => [
                    {
                      type => 'parbreak'
                    },
                    {
                      type => 'text',
                      content => 'USB On The Go FS global interrupt '
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'dma.c',
      includes => [
        {
          name => 'dma.h',
          ref => 'dma_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'dma_channel_reset',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_mem2mem_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_priority',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'prio',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_memory_size',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'mem_size',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_peripheral_size',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'peripheral_size',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_memory_increment_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_peripheral_increment_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_circular_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_read_from_peripheral',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_read_from_memory',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_transfer_error_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_transfer_error_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_half_transfer_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_half_transfer_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_transfer_complete_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_transfer_complete_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_channel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_channel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_peripheral_address',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'address',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_memory_address',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'address',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_number_of_data',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'number',
                type => 'u16'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'dma.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/dma.c',
          ref => 'dma_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'DMA1_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(AHBPERIPH_BASE  + 0x0000)'
          },
          {
            kind => 'define',
            name => 'DMA2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(AHBPERIPH_BASE  + 0x0400)'
          },
          {
            kind => 'define',
            name => 'DMA1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_BASE'
          },
          {
            kind => 'define',
            name => 'DMA2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_BASE'
          },
          {
            kind => 'define',
            name => 'DMA_ISR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x00)'
          },
          {
            kind => 'define',
            name => 'DMA1_ISR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR(DMA1)'
          },
          {
            kind => 'define',
            name => 'DMA2_ISR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR(DMA2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x04)'
          },
          {
            kind => 'define',
            name => 'DMA1_IFCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR(DMA1)'
          },
          {
            kind => 'define',
            name => 'DMA2_IFCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR(DMA2)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x08 + \\
					       (0x14 * ((channel) - 1)))'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CCR(DMA1, channel)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA1_CCR7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CCR(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CCR(DMA2, channel)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CCR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CCR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CCR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CCR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA2_CCR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CCR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_CNDTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x0C + \\
					       (0x14 * ((channel) - 1)))'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CNDTR(DMA1, channel)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA1_CNDTR7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CNDTR(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CNDTR(DMA2, channel)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CNDTR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CNDTR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CNDTR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CNDTR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA2_CNDTR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CNDTR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_CPAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x10 + \\
					       (0x14 * ((channel) - 1)))'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CPAR(DMA1, channel)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA1_CPAR7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CPAR(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CPAR(DMA2, channel)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CPAR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CPAR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CPAR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CPAR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA2_CPAR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CPAR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_CMAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'dma_base'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'MMIO32(dma_base + 0x14 + \\
					       (0x14 * ((channel) - 1)))'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CMAR(DMA1, channel)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA1_CMAR7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA1_CMAR(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => 'DMA_CMAR(DMA2, channel)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CMAR(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CMAR(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CMAR(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CMAR(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA2_CMAR5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA2_CMAR(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 3)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_ISR_TEIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TEIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TEIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 2)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_ISR_HTIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_HTIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_HTIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 1)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_ISR_TCIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_TCIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_TCIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 0)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_ISR_GIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_ISR_GIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_ISR_GIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_IFCR_CTEIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTEIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTEIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_IFCR_CHTIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CHTIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CHTIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_IFCR_CTCIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CTCIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CTCIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 0)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_IFCR_CGIF_BIT << (4 * ((channel) -1)))'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CGIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CGIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF_BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0xF'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'channel'
              }
            ],
            initializer => '(DMA_IFCR_CIF_BIT << (4 * ((channel) - 1)))'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL1)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL2)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL3)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL4)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL5)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL6)'
          },
          {
            kind => 'define',
            name => 'DMA_IFCR_CIF7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'DMA_IFCR_CIF(DMA_CHANNEL7)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MEM2MEM',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 14)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_LOW',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x0 << 12)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_MEDIUM',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x1 << 12)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_HIGH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x2 << 12)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_VERY_HIGH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x3 << 12)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_MASK',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x3 << 12)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PL_SHIFT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '12'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MSIZE_8BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x0 << 10)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MSIZE_16BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x1 << 10)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MSIZE_32BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x2 << 10)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MSIZE_MASK',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x3 << 10)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MSIZE_SHIFT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '10'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PSIZE_8BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x0 << 8)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PSIZE_16BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x1 << 8)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PSIZE_32BIT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x2 << 8)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PSIZE_MASK',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(0x3 << 8)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PSIZE_SHIFT',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '8'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_MINC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 7)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_PINC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 6)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_CIRC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 5)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_DIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 4)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_TEIE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 3)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_HTIE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 2)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_TCIE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 1)'
          },
          {
            kind => 'define',
            name => 'DMA_CCR_EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(1 << 0)'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '3'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '4'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '5'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '6'
          },
          {
            kind => 'define',
            name => 'DMA_CHANNEL7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '7'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'dma_channel_reset',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_mem2mem_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_priority',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'prio',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_memory_size',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'mem_size',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_peripheral_size',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'peripheral_size',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_memory_increment_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_peripheral_increment_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_circular_mode',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_read_from_peripheral',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_read_from_memory',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_transfer_error_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_transfer_error_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'uint32_t'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_half_transfer_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_half_transfer_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_transfer_complete_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_transfer_complete_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_enable_channel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_disable_channel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_peripheral_address',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'address',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_memory_address',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'address',
                type => 'u32'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_set_number_of_data',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'dma',
                type => 'u32'
              },
              {
                declaration_name => 'channel',
                type => 'u8'
              },
              {
                declaration_name => 'number',
                type => 'u16'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'extint.c',
      includes => [
        {
          name => 'extint.h',
          ref => 'extint_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'EXTInterruptPinEnable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Pin Enable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This enable to choose which pin to enable in the external interrupt between pin 0 - 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'pin'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. pin number values 0-15. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              },
              {
                declaration_name => 'pin',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'EXTInterruptEnable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Enable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This enable external interrupt from 0 to 15 and allow to choose to interrupt in rising edge or faling edge or both.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'rising'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. enable or disable interrupt at rising edge True or False. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'failing'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. enable or disable interrupt at failing edge True or False. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              },
              {
                declaration_name => 'rising',
                type => 'char'
              },
              {
                declaration_name => 'failing',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'EXTInterruptDisable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Disable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This disable external interrupt from 0 to 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15\\ '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'resetExternalInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Reset External Interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This reset external interrupt from 0 to 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15\\ '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'extint.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/extint.c',
          ref => 'extint_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'EXTI',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((EXTI_TypeDef   *) EXTI_BASE    )'
          },
          {
            kind => 'define',
            name => 'EXTI0',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'EXTI1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'EXTI2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'EXTI3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '3'
          },
          {
            kind => 'define',
            name => 'EXTI4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '4'
          },
          {
            kind => 'define',
            name => 'EXTI5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '5'
          },
          {
            kind => 'define',
            name => 'EXTI6',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '6'
          },
          {
            kind => 'define',
            name => 'EXTI7',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '7'
          },
          {
            kind => 'define',
            name => 'EXTI8',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '8'
          },
          {
            kind => 'define',
            name => 'EXTI9',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '9'
          },
          {
            kind => 'define',
            name => 'EXTI10',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '10'
          },
          {
            kind => 'define',
            name => 'EXTI11',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '11'
          },
          {
            kind => 'define',
            name => 'EXTI12',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '12'
          },
          {
            kind => 'define',
            name => 'EXTI13',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '13'
          },
          {
            kind => 'define',
            name => 'EXTI14',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '14'
          },
          {
            kind => 'define',
            name => 'EXTI15',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '15'
          },
          {
            kind => 'define',
            name => 'EXTI16',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '16'
          },
          {
            kind => 'define',
            name => 'EXTI17',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '17'
          },
          {
            kind => 'define',
            name => 'EXTI18',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '18'
          },
          {
            kind => 'define',
            name => 'EXTI19',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '19'
          },
          {
            kind => 'define',
            name => 'PA',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'PB',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'PC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'PD',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '3'
          },
          {
            kind => 'define',
            name => 'PE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '4'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'EXTInterruptPinEnable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Pin Enable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This enable to choose which pin to enable in the external interrupt between pin 0 - 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'pin'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. pin number values 0-15. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              },
              {
                declaration_name => 'pin',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'EXTInterruptEnable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Enable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This enable external interrupt from 0 to 15 and allow to choose to interrupt in rising edge or faling edge or both.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'rising'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. enable or disable interrupt at rising edge True or False. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'failing'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. enable or disable interrupt at failing edge True or False. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              },
              {
                declaration_name => 'rising',
                type => 'char'
              },
              {
                declaration_name => 'failing',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'EXTInterruptDisable',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'External Interrupt Disable. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This disable external interrupt from 0 to 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15\\ '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'resetExternalInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Reset External Interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This reset external interrupt from 0 to 15.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'interrupt_number'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. External interrupt number values 0-15\\ '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'interrupt_number',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'gpio.c',
      includes => [
        {
          name => 'gpio.h',
          ref => 'gpio_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'portInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => '__val',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portInitAlt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT initialization Alternative Function (AF). '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This initialize the GPIO enables it\'s clock Specify Enabling the Alternative Function (AF)for the desired GPIO pin'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'wether the value was 1 then it\'s Output Push-Pull if 0 keep it as it was previously initialized '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => '_val',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portWrite',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT Write Output. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'Specify Output for the desired GPIO pin or many pins in the same GPIO'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Logic 0 or 1 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => 'value',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portRead',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT Read Input. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'Specify Output for the desired GPIO pin or many pins in the same GPIO'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'pin'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'position in the GPIO from [0 to 15]'
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'the'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'return value Logic 0 or 1 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'uint16_t',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => 'pin',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'Set_clock',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO Set Clock. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'Specify Enabling the clock gating for the desired GPIO PORT'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              }
            ]
          }
        ]
      },
      variables => {
        members => [
          {
            kind => 'variable',
            name => '__y',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO source file. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This source file should include functions to do the following  Initialize  Read Input  Write OutputGPIO PORT initialization.'
                },
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the GPIO enables it\'s clock Specify the mode IN or OUT specify disable AF Disable Pullups Resistors Enable Clock gating'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => '16-bits'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'value spevify the desired mode of each pin 1 for Output with max clock gating[50MHz] 0 for Input floating. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'unsigned long',
            initializer => '=0'
          },
          {
            kind => 'variable',
            name => '__z',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned long',
            initializer => '=0'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'gpio.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/can.h',
          ref => 'can_8h'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/gpio.c',
          ref => 'gpio_8c'
        },
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/usart.h',
          ref => 'usart_8h'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'GPIOA',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((GPIO_TypeDef   *) GPIOA_BASE   )'
          },
          {
            kind => 'define',
            name => 'GPIOB',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((GPIO_TypeDef   *) GPIOB_BASE   )'
          },
          {
            kind => 'define',
            name => 'GPIOC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((GPIO_TypeDef   *) GPIOC_BASE   )'
          },
          {
            kind => 'define',
            name => 'GPIOD',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((GPIO_TypeDef   *) GPIOD_BASE   )'
          },
          {
            kind => 'define',
            name => 'GPIOE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((GPIO_TypeDef   *) GPIOE_BASE   )'
          },
          {
            kind => 'define',
            name => 'PORTA',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->ODR'
          },
          {
            kind => 'define',
            name => 'PINA',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->IDR'
          },
          {
            kind => 'define',
            name => 'PORTA_MODEL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->CRL'
          },
          {
            kind => 'define',
            name => 'PORTA_MODEH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->CRH'
          },
          {
            kind => 'define',
            name => 'PORTA_BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->BSRR'
          },
          {
            kind => 'define',
            name => 'PORTA_BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->BRR'
          },
          {
            kind => 'define',
            name => 'PORTA_LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOA->LCKR'
          },
          {
            kind => 'define',
            name => 'PORTB',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->ODR'
          },
          {
            kind => 'define',
            name => 'PINB',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->IDR'
          },
          {
            kind => 'define',
            name => 'PORTB_MODEL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->CRL'
          },
          {
            kind => 'define',
            name => 'PORTB_MODEH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->CRH'
          },
          {
            kind => 'define',
            name => 'PORTB_BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->BSRR'
          },
          {
            kind => 'define',
            name => 'PORTB_BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->BRR'
          },
          {
            kind => 'define',
            name => 'PORTB_LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOB->LCKR'
          },
          {
            kind => 'define',
            name => 'PORTC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->ODR'
          },
          {
            kind => 'define',
            name => 'PINC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->IDR'
          },
          {
            kind => 'define',
            name => 'PORTC_MODEL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->CRL'
          },
          {
            kind => 'define',
            name => 'PORTC_MODEH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->CRH'
          },
          {
            kind => 'define',
            name => 'PORTC_BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->BSRR'
          },
          {
            kind => 'define',
            name => 'PORTC_BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->BRR'
          },
          {
            kind => 'define',
            name => 'PORTC_LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOC->LCKR'
          },
          {
            kind => 'define',
            name => 'PORTD',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->ODR'
          },
          {
            kind => 'define',
            name => 'PIND',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->IDR'
          },
          {
            kind => 'define',
            name => 'PORTD_CTRL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->CRL'
          },
          {
            kind => 'define',
            name => 'PORTD_CRH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->CRH'
          },
          {
            kind => 'define',
            name => 'PORTD_BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->BSRR'
          },
          {
            kind => 'define',
            name => 'PORTD_BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->BRR'
          },
          {
            kind => 'define',
            name => 'PORTD_LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOD->LCKR'
          },
          {
            kind => 'define',
            name => 'PORTE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->ODR'
          },
          {
            kind => 'define',
            name => 'PINE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->IDR'
          },
          {
            kind => 'define',
            name => 'PORTE_MODEL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->CRL'
          },
          {
            kind => 'define',
            name => 'PORTE_MODEH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->CRH'
          },
          {
            kind => 'define',
            name => 'PORTE_BSRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->BSRR'
          },
          {
            kind => 'define',
            name => 'PORTE_BRR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->BRR'
          },
          {
            kind => 'define',
            name => 'PORTE_LCKR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'GPIOE->LCKR'
          },
          {
            kind => 'define',
            name => 'CLOCK_BUS2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'RCC->APB2ENR'
          },
          {
            kind => 'define',
            name => 'PORTA_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 2'
          },
          {
            kind => 'define',
            name => 'PORTB_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 3'
          },
          {
            kind => 'define',
            name => 'PORTC_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 4'
          },
          {
            kind => 'define',
            name => 'PORTD_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 5'
          },
          {
            kind => 'define',
            name => 'PORTE_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 6'
          },
          {
            kind => 'define',
            name => 'AFIO_CLOCKEN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1 << 0'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'portInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'PT',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => '__val',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portInitAlt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT initialization Alternative Function (AF). '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the GPIO enables it\'s clock Specify Enabling the Alternative Function (AF)for the desired GPIO pin'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'wether the value was 1 then it\'s Output Push-Pull if 0 keep it as it was previously initialized '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'PT',
                definition_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => '_val',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portRead',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT Read Input. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Specify Output for the desired GPIO pin or many pins in the same GPIO'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'pin'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'position in the GPIO from [0 to 15]'
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'the'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'return value Logic 0 or 1 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'uint16_t',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'PORT',
                definition_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => 'pin',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'Set_clock',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO Set Clock. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Specify Enabling the clock gating for the desired GPIO PORT'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'GIO',
                definition_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              }
            ]
          },
          {
            kind => 'function',
            name => 'portWrite',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'GPIO PORT Write Output. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Specify Output for the desired GPIO pin or many pins in the same GPIO'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'GPIOx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e GPIOA '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Logic 0 or 1 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'PORT',
                definition_name => 'GPIO',
                type => 'GPIO_TypeDef *'
              },
              {
                declaration_name => 'value',
                type => 'uint16_t'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'nvic.c',
      includes => [
        {
          name => 'nvic.h',
          ref => 'nvic_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'nvic_enable_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_disable_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_set_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_clear_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_active_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_irq_enabled',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_set_priority',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              },
              {
                declaration_name => 'priority',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_generate_software_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'nvic.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/nvic.c',
          ref => 'nvic_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'NVIC_WWDG_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'NVIC_PVD_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'NVIC_TAMPER_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'NVIC_RTC_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '3'
          },
          {
            kind => 'define',
            name => 'NVIC_FLASH_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '4'
          },
          {
            kind => 'define',
            name => 'NVIC_RCC_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '5'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI0_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '6'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '7'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '8'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '9'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI4_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '10'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '11'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '12'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '13'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL4_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '14'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '15'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL6_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '16'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA1_CHANNEL7_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '17'
          },
          {
            kind => 'define',
            name => 'NVIC_ADC1_2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '18'
          },
          {
            kind => 'define',
            name => 'NVIC_USB_HP_CAN_TX_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '19'
          },
          {
            kind => 'define',
            name => 'NVIC_USB_LP_CAN_RX0_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '20'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN_RX1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '21'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN_SCE_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '22'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI9_5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '23'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM1_BRK_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '24'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM1_UP_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '25'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM1_TRG_COM_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '26'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM1_CC_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '27'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '28'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '29'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM4_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '30'
          },
          {
            kind => 'define',
            name => 'NVIC_I2C1_EV_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '31'
          },
          {
            kind => 'define',
            name => 'NVIC_I2C1_ER_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '32'
          },
          {
            kind => 'define',
            name => 'NVIC_I2C2_EV_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '33'
          },
          {
            kind => 'define',
            name => 'NVIC_I2C2_ER_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '34'
          },
          {
            kind => 'define',
            name => 'NVIC_SPI1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '35'
          },
          {
            kind => 'define',
            name => 'NVIC_SPI2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '36'
          },
          {
            kind => 'define',
            name => 'NVIC_USART1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '37'
          },
          {
            kind => 'define',
            name => 'NVIC_USART2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '38'
          },
          {
            kind => 'define',
            name => 'NVIC_USART3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '39'
          },
          {
            kind => 'define',
            name => 'NVIC_EXTI15_10_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '40'
          },
          {
            kind => 'define',
            name => 'NVIC_RTC_ALARM_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '41'
          },
          {
            kind => 'define',
            name => 'NVIC_USB_WAKEUP_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '42'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM8_BRK_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '43'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM8_UP_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '44'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM8_TRG_COM_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '45'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM8_CC_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '46'
          },
          {
            kind => 'define',
            name => 'NVIC_ADC3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '47'
          },
          {
            kind => 'define',
            name => 'NVIC_FSMC_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '48'
          },
          {
            kind => 'define',
            name => 'NVIC_SDIO_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '49'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '50'
          },
          {
            kind => 'define',
            name => 'NVIC_SPI3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '51'
          },
          {
            kind => 'define',
            name => 'NVIC_UART4_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '52'
          },
          {
            kind => 'define',
            name => 'NVIC_UART5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '53'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM6_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '54'
          },
          {
            kind => 'define',
            name => 'NVIC_TIM7_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '55'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA2_CHANNEL1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '56'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA2_CHANNEL2_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '57'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA2_CHANNEL3_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '58'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA2_CHANNEL4_5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '59'
          },
          {
            kind => 'define',
            name => 'NVIC_DMA2_CHANNEL5_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '60'
          },
          {
            kind => 'define',
            name => 'NVIC_ETH_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '61'
          },
          {
            kind => 'define',
            name => 'NVIC_ETH_WKUP_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '62'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN2_TX_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '63'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN2_RX0_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '64'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN2_RX1_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '65'
          },
          {
            kind => 'define',
            name => 'NVIC_CAN2_SCE_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '66'
          },
          {
            kind => 'define',
            name => 'NVIC_OTG_FS_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '67'
          },
          {
            kind => 'define',
            name => 'NVIC_ISER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'iser_id'
              }
            ],
            initializer => 'MMIO32(NVIC_BASE + 0x00 + (iser_id * 4))'
          },
          {
            kind => 'define',
            name => 'NVIC_ICER',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'icer_id'
              }
            ],
            initializer => 'MMIO32(NVIC_BASE + 0x80 + (icer_id * 4))'
          },
          {
            kind => 'define',
            name => 'NVIC_ISPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'ispr_id'
              }
            ],
            initializer => 'MMIO32(NVIC_BASE + 0x100 + (ispr_id * 4))'
          },
          {
            kind => 'define',
            name => 'NVIC_ICPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'icpr_id'
              }
            ],
            initializer => 'MMIO32(NVIC_BASE + 0x180 + (icpr_id * 4))'
          },
          {
            kind => 'define',
            name => 'NVIC_IABR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'iabr_id'
              }
            ],
            initializer => 'MMIO32(NVIC_BASE + 0x200 + (iabr_id * 4))'
          },
          {
            kind => 'define',
            name => 'NVIC_IPR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'ipr_id'
              }
            ],
            initializer => 'MMIO8(NVIC_BASE + 0x300 + ipr_id)'
          },
          {
            kind => 'define',
            name => 'NVIC_STIR',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => 'MMIO32(STIR_BASE)'
          },
          {
            kind => 'define',
            name => 'NVIC_NMI_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-14'
          },
          {
            kind => 'define',
            name => 'NVIC_HARD_FAULT_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-13'
          },
          {
            kind => 'define',
            name => 'NVIC_MEM_MANAGE_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-12'
          },
          {
            kind => 'define',
            name => 'NVIC_BUS_FAULT_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-11'
          },
          {
            kind => 'define',
            name => 'NVIC_USAGE_FAULT_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-10'
          },
          {
            kind => 'define',
            name => 'NVIC_SV_CALL_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-5'
          },
          {
            kind => 'define',
            name => 'DEBUG_MONITOR_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-4'
          },
          {
            kind => 'define',
            name => 'NVIC_PENDSV_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-2'
          },
          {
            kind => 'define',
            name => 'NVIC_SYSTICK_IRQ',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '-1'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'nvic_enable_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_disable_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_set_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_clear_pending_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_active_irq',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_get_irq_enabled',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'u8',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_set_priority',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              },
              {
                declaration_name => 'priority',
                type => 'u8'
              }
            ]
          },
          {
            kind => 'function',
            name => 'nvic_generate_software_interrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'irqn',
                type => 'u8'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'timer.c',
      includes => [
        {
          name => 'timer.h',
          ref => 'timer_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'timerInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'text',
                  content => 'Timer initialization. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This initialize the timer enable it\'s clock, enable alternative function clock, the clock to the GPIO, Select up counting mode and Internal Clock. note this will set update event flag.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler values 0...0xFFFF. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'prescaler',
                type => 'unsigned int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'advancedTimerInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'text',
                  content => 'Advanced timer initialization. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the timer enable it\'s clock, enable alternative function clock and the clock to the GPIO, Allow you to Select counting mode and Clock Source. note this will set update event flag.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'source'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'unsigned int. source values INTERNAL,EXTERNAL,EM1,EM2,EM3 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'dir'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. direction values UP or DOWN. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'prescaler',
                type => 'unsigned int'
              },
              {
                declaration_name => 'source',
                type => 'unsigned int'
              },
              {
                declaration_name => 'dir',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerIC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer input capture. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize one channel of a timer as input capture, Allow you to Select Edge detection as rising or falling edge.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'edge'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. edge values RISING or FALLING. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              },
              {
                declaration_name => 'edge',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerOC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer output compare. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize one channel of a timer as output compare to use as PWM, Allow you to Select the peroid of the PWM and it\'s duty cycle.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'period'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. period values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'period',
                type => 'unsigned int'
              },
              {
                declaration_name => 'duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerOC4CH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer output compare for the 4 channels. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize thr four channel of a timer as output compare to use as PWM, Allow you to Select the peroid of the PWM and it\'s duty cycle.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'period'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. period values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle1'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle1 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle2'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle2 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle3'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle3 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle4'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle4 values 0...0xFFFF. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'period',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel1_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel2_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel3_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel4_duty_cycle',
                type => 'unsigned int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'readCapture',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'text',
                  content => 'Read input capture value. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This wait until the input capture detect edge and input capture store the timer value in Capture Compare Register CCR and return its value.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'unsigned int. values 0...0xFFFF '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'unsigned int',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'enableTimerInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Enable timer interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This enable timer interrupt for one channel giving you the choise to enable or disable update event interrupt.'
                },
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'note: to enable or disable update event interrupt only set channel to 0'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'update'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. update values 0 or 1. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'update',
                type => 'char'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'disableTimerInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Disable timer interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This disable timer interrupt for one channel.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'timer.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/timer.c',
          ref => 'timer_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'INTERNAL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'EM1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'EM2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '2'
          },
          {
            kind => 'define',
            name => 'EM3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '3'
          },
          {
            kind => 'define',
            name => 'EXTERNAL',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x4000'
          },
          {
            kind => 'define',
            name => 'RISING',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'FALLING',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'UP',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0'
          },
          {
            kind => 'define',
            name => 'DOWN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1'
          },
          {
            kind => 'define',
            name => 'DIRECTION',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '4'
          },
          {
            kind => 'define',
            name => 'setTimerMax',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              },
              {
                name => 'autoreload'
              }
            ],
            initializer => 'TIMER->ARR =autoreload'
          },
          {
            kind => 'define',
            name => 'setTimerCount',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              },
              {
                name => 'count'
              }
            ],
            initializer => 'TIMER->CNT =count'
          },
          {
            kind => 'define',
            name => 'timerCount',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->CNT'
          },
          {
            kind => 'define',
            name => 'reinitTimer',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->EGR =1'
          },
          {
            kind => 'define',
            name => 'resetTimerStatusUpdate',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->SR &= ~1'
          },
          {
            kind => 'define',
            name => 'resetTimerStatusChannel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'TIMER->SR &= ~(1<<channel)'
          },
          {
            kind => 'define',
            name => 'timerStatusUpdate',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->SR & 1'
          },
          {
            kind => 'define',
            name => 'timerStatusChannel',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              },
              {
                name => 'channel'
              }
            ],
            initializer => 'TIMER->SR &= (1<<channel)'
          },
          {
            kind => 'define',
            name => 'readCaptureValueCH1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->CCR1'
          },
          {
            kind => 'define',
            name => 'readCaptureValueCH2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->CCR2'
          },
          {
            kind => 'define',
            name => 'readCaptureValueCH3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->CCR3'
          },
          {
            kind => 'define',
            name => 'readCaptureValueCH4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
              {
                name => 'TIMER'
              }
            ],
            initializer => 'TIMER->CCR4'
          },
          {
            kind => 'define',
            name => 'TIM2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((TIM_GP_TypeDef *) TIM2_BASE    )'
          },
          {
            kind => 'define',
            name => 'TIM3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((TIM_GP_TypeDef *) TIM3_BASE    )'
          },
          {
            kind => 'define',
            name => 'TIM4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((TIM_GP_TypeDef *) TIM4_BASE    )'
          },
          {
            kind => 'define',
            name => 'TIM5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((TIM_GP_TypeDef *) TIM5_BASE    )'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'timerInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Timer initialization. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the timer enable it\'s clock, enable alternative function clock, the clock to the GPIO, Select up counting mode and Internal Clock. note this will set update event flag.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler values 0...0xFFFF. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'prescaler',
                type => 'unsigned int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'advancedTimerInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Advanced timer initialization. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the timer enable it\'s clock, enable alternative function clock and the clock to the GPIO, Allow you to Select counting mode and Clock Source. note this will set update event flag.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'source'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'unsigned int. source values INTERNAL,EXTERNAL,EM1,EM2,EM3 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'dir'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. direction values UP or DOWN. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'prescaler',
                type => 'unsigned int'
              },
              {
                declaration_name => 'source',
                type => 'unsigned int'
              },
              {
                declaration_name => 'dir',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerIC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer input capture. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize one channel of a timer as input capture, Allow you to Select Edge detection as rising or falling edge.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'edge'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. edge values RISING or FALLING. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              },
              {
                declaration_name => 'edge',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerOC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer output compare. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize one channel of a timer as output compare to use as PWM, Allow you to Select the peroid of the PWM and it\'s duty cycle.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'period'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. period values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'period',
                type => 'unsigned int'
              },
              {
                declaration_name => 'duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'initTimerOC4CH',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initializetimer output compare for the 4 channels. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize thr four channel of a timer as output compare to use as PWM, Allow you to Select the peroid of the PWM and it\'s duty cycle.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'period'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'Unsigned int. period values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle1'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle1 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle2'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle2 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle3'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle3 values 0...0xFFFF. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'duty_cycle4'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. duty_cycle4 values 0...0xFFFF. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'period',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel1_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel2_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel3_duty_cycle',
                type => 'unsigned int'
              },
              {
                declaration_name => 'chnnel4_duty_cycle',
                type => 'unsigned int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'readCapture',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Read input capture value. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This wait until the input capture detect edge and input capture store the timer value in Capture Compare Register CCR and return its value.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'value'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'unsigned int. values 0...0xFFFF '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'unsigned int',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'enableTimerInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Enable timer interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This enable timer interrupt for one channel giving you the choise to enable or disable update event interrupt.'
                },
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'note: to enable or disable update event interrupt only set channel to 0'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'update'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. update values 0 or 1. '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'update',
                type => 'char'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'disableTimerInterrupt',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Disable timer interrupt. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This disable timer interrupt for one channel.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'TIMER'
                      }
                    ],
                    doc => [
                      {
                        type => 'url',
                        link => 'struct_t_i_m___g_p___type_def',
                        content => 'TIM_GP_TypeDef'
                      },
                      {
                        type => 'text',
                        content => '. Timer register address base '
                      },
                      {
                        type => 'ref',
                        content => [
                          {
                            type => 'text',
                            content => 'TIMx_BASE'
                          }
                        ]
                      },
                      {
                        type => 'text',
                        content => ' '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'channel'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'char. channel values 1-4 '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'TIMER',
                type => 'TIM_GP_TypeDef *'
              },
              {
                declaration_name => 'channel',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'usart.c',
      includes => [
        {
          name => 'usart.h',
          ref => 'usart_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'usartInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'usart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'br',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'sendChar',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'uart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'ch',
                type => 'unsigned char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'GetChar',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'urt',
                type => 'USART_TypeDef *'
              }
            ]
          },
          {
            kind => 'function',
            name => 'Send_String',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'uart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'str',
                type => 'unsigned char *'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_write_usart1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_read_usart1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_write_usart2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_read_usart2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_write_usart3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_read_usart3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_write_usart4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_read_usart4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          }
        ]
      },
      variables => {
        members => [
          {
            kind => 'variable',
            name => 'rate',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'USART source file. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'This source file should include functions to do the following  Initialize  Read Character  Write Character  Write StringUSART initialization.'
                },
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This initialize the GPIO enables it\'s clock Specify the mode of TX & RX pins as Alt. Push-Pull and IN Floating Baud Rate Divisor Enable USART Clock Gating Enable USART | Enable Tx & Rx Functionality Enable/Disable DMA Functionality'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'USARTx.'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'i.e USART1 '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => '16-bits'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'value specify the desired Baud Rate. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'double'
          },
          {
            kind => 'variable',
            name => 'Div',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'double'
          },
          {
            kind => 'variable',
            name => '__clk',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'int'
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'usart.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        },
        {
          name => 'gpio.h',
          ref => 'gpio_8h'
        },
        {
          name => 'math.h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/usart.c',
          ref => 'usart_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'USART2_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4400)'
          },
          {
            kind => 'define',
            name => 'USART3_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4800)'
          },
          {
            kind => 'define',
            name => 'USART4_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x4C00)'
          },
          {
            kind => 'define',
            name => 'USART5_BASE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '(APB1PERIPH_BASE + 0x5000)'
          },
          {
            kind => 'define',
            name => 'USART1',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((USART_TypeDef  *) USART1_BASE  )'
          },
          {
            kind => 'define',
            name => 'USART2',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((USART_TypeDef  *) USART2_BASE  )'
          },
          {
            kind => 'define',
            name => 'USART3',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((USART_TypeDef  *) USART3_BASE  )'
          },
          {
            kind => 'define',
            name => 'USART4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((USART_TypeDef  *) USART4_BASE  )'
          },
          {
            kind => 'define',
            name => 'USART5',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((USART_TypeDef  *) USART5_BASE  )'
          },
          {
            kind => 'define',
            name => 'USART_SR_PE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'text',
                  content => 'Parity Error '
                }
              ]
            },
            initializer => '((uint16_t)0x0001)'
          },
          {
            kind => 'define',
            name => 'USART_SR_FE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Framing Error '
                }
              ]
            },
            initializer => '((uint16_t)0x0002)'
          },
          {
            kind => 'define',
            name => 'USART_SR_NE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Noise Error Flag '
                }
              ]
            },
            initializer => '((uint16_t)0x0004)'
          },
          {
            kind => 'define',
            name => 'USART_SR_ORE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'OverRun Error '
                }
              ]
            },
            initializer => '((uint16_t)0x0008)'
          },
          {
            kind => 'define',
            name => 'USART_SR_IDLE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'IDLE line detected '
                }
              ]
            },
            initializer => '((uint16_t)0x0010)'
          },
          {
            kind => 'define',
            name => 'USART_SR_RXNE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Read Data Register Not Empty '
                }
              ]
            },
            initializer => '((uint16_t)0x0020)'
          },
          {
            kind => 'define',
            name => 'USART_SR_TC',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Transmission Complete '
                }
              ]
            },
            initializer => '((uint16_t)0x0040)'
          },
          {
            kind => 'define',
            name => 'USART_SR_TXE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Transmit Data Register Empty '
                }
              ]
            },
            initializer => '((uint16_t)0x0080)'
          },
          {
            kind => 'define',
            name => 'USART_SR_LBD',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'LIN Break Detection Flag '
                }
              ]
            },
            initializer => '((uint16_t)0x0100)'
          },
          {
            kind => 'define',
            name => 'USART_SR_CTS',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {}
          },
          {
            kind => 'define',
            name => 'RCC_APB2ENR_USART1EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<14'
          },
          {
            kind => 'define',
            name => 'RCC_APB1ENR_USART2EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<17'
          },
          {
            kind => 'define',
            name => 'RCC_APB1ENR_USART3EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<18'
          },
          {
            kind => 'define',
            name => 'RCC_APB1ENR_USART4EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<19'
          },
          {
            kind => 'define',
            name => 'RCC_APB1ENR_USART5EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<20'
          },
          {
            kind => 'define',
            name => 'USART_CR1_RE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<2'
          },
          {
            kind => 'define',
            name => 'USART_CR1_TE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<3'
          },
          {
            kind => 'define',
            name => 'USART_CR1_UE',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<13'
          },
          {
            kind => 'define',
            name => 'USART_DMA_EN',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '1<<7'
          },
          {
            kind => 'define',
            name => 'USART_DMA_DIS',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '~(1<<7)'
          },
          {
            kind => 'define',
            name => 'fclk',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '8'
          },
          {
            kind => 'define',
            name => 'Baud',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '9600'
          },
          {
            kind => 'define',
            name => 'Div',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((fclk) / (16 * (Baud/pow(1000,2))))'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'usartInit',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'usart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'br',
                type => 'uint16_t'
              }
            ]
          },
          {
            kind => 'function',
            name => 'usart1Setup',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                type => 'void'
              }
            ]
          },
          {
            kind => 'function',
            name => 'Send_String',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'uart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'str',
                type => 'unsigned char *'
              }
            ]
          },
          {
            kind => 'function',
            name => 'GetChar',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'unsigned char',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'urt',
                type => 'USART_TypeDef *'
              }
            ]
          },
          {
            kind => 'function',
            name => 'SendChar',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'uart',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'ch',
                type => 'unsigned char'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_read',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => '__USART',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          },
          {
            kind => 'function',
            name => 'dma_write',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => '_USART',
                type => 'USART_TypeDef *'
              },
              {
                declaration_name => 'data',
                type => 'char *'
              },
              {
                declaration_name => 'size',
                type => 'int'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'watchdog.c',
      includes => [
        {
          name => 'watchdog.h',
          ref => 'watchdog_8h'
        }
      ],
      included_by => [
      ],
      functions => {
        members => [
          {
            kind => 'function',
            name => 'initWatchDog',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initialize WatchDog. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This Start WatchDog timer with reload value and prescaler PS Independent WatchDog has its own clock source with 32KHz.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'reload'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'unsigned int. WatchDog timer reload value Value 0x00-0xFFF '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'reload',
                type => 'unsigned int'
              },
              {
                declaration_name => 'prescaler',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    },
    {
      name => 'watchdog.h',
      includes => [
        {
          name => 'common.h',
          ref => 'common_8h'
        }
      ],
      included_by => [
        {
          name => 'C:/Users/Eng. Mohamed/Desktop/ARTRONIC/Library/watchdog.c',
          ref => 'watchdog_8c'
        }
      ],
      defines => {
        members => [
          {
            kind => 'define',
            name => 'PRESCALE_4',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x0'
          },
          {
            kind => 'define',
            name => 'PRESCALE_8',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x1'
          },
          {
            kind => 'define',
            name => 'PRESCALE_16',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x2'
          },
          {
            kind => 'define',
            name => 'PRESCALE_32',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x3'
          },
          {
            kind => 'define',
            name => 'PRESCALE_64',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x4'
          },
          {
            kind => 'define',
            name => 'PRESCALE_128',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x5'
          },
          {
            kind => 'define',
            name => 'PRESCALE_256',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '0x6'
          },
          {
            kind => 'define',
            name => 'resetWatchDog',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            parameters => [
            ],
            initializer => 'IWDG->KR=0xAAAA'
          },
          {
            kind => 'define',
            name => 'IWDG',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {},
            detailed => {},
            initializer => '((IWDG_TypeDef   *) IWDG_BASE    )'
          }
        ]
      },
      functions => {
        members => [
          {
            kind => 'function',
            name => 'initWatchDog',
            virtualness => 'non_virtual',
            protection => 'public',
            static => 'no',
            brief => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'Initialize WatchDog. '
                }
              ]
            },
            detailed => {
              doc => [
                {
                  type => 'parbreak'
                },
                {
                  type => 'text',
                  content => 'This Start WatchDog timer with reload value and prescaler PS Independent WatchDog has its own clock source with 32KHz.'
                },
                {
                  type => 'parbreak'
                },
                params => [
                  {
                    parameters => [
                      {
                        name => 'reload'
                      }
                    ],
                    doc => [
                      {
                        type => 'text',
                        content => 'unsigned int. WatchDog timer reload value Value 0x00-0xFFF '
                      }
                    ]
                  },
                  {
                    parameters => [
                      {
                        name => 'prescaler'
                      }
                    ],
                    doc => [
                      {
                        type => 'parbreak'
                      },
                      {
                        type => 'text',
                        content => 'Unsigned int. Prescaler. '
                      }
                    ]
                  }
                ]
              ]
            },
            type => 'void',
            const => 'no',
            volatile => 'no',
            parameters => [
              {
                declaration_name => 'reload',
                type => 'unsigned int'
              },
              {
                declaration_name => 'prescaler',
                type => 'char'
              }
            ]
          }
        ]
      },
      brief => {},
      detailed => {}
    }
  ],
  groups => [
  ],
  pages => [
  ]
};
1;
