DOXY_EXEC_PATH = C:/Users/Eng. Mohamed/Desktop/dc
DOXYFILE = C:/Users/Eng. Mohamed/Desktop/dc/-
DOXYDOCS_PM = C:/Users/Eng. Mohamed/Desktop/dc/perlmod/DoxyDocs.pm
DOXYSTRUCTURE_PM = C:/Users/Eng. Mohamed/Desktop/dc/perlmod/DoxyStructure.pm
DOXYRULES = C:/Users/Eng. Mohamed/Desktop/dc/perlmod/doxyrules.make

.PHONY: clean-perlmod
clean-perlmod::
	rm -f $(DOXYSTRUCTURE_PM) \
	$(DOXYDOCS_PM)

$(DOXYRULES) \
$(DOXYMAKEFILE) \
$(DOXYSTRUCTURE_PM) \
$(DOXYDOCS_PM): \
	$(DOXYFILE)
	cd $(DOXY_EXEC_PATH) ; doxygen "$<"
