var searchData=
[
  ['nvic_5fclear_5fpending_5firq',['nvic_clear_pending_irq',['../nvic_8c.html#a912739401591c8c16d54b68bf6a19145',1,'nvic_clear_pending_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a912739401591c8c16d54b68bf6a19145',1,'nvic_clear_pending_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fdisable_5firq',['nvic_disable_irq',['../nvic_8c.html#aed0997045521d8dd7171a1efb9735b2f',1,'nvic_disable_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#aed0997045521d8dd7171a1efb9735b2f',1,'nvic_disable_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fenable_5firq',['nvic_enable_irq',['../nvic_8c.html#a2a1ec26fc4559447108f42299d0134a7',1,'nvic_enable_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a2a1ec26fc4559447108f42299d0134a7',1,'nvic_enable_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fgenerate_5fsoftware_5finterrupt',['nvic_generate_software_interrupt',['../nvic_8c.html#a2106013b3082b25589ccf64ade71d837',1,'nvic_generate_software_interrupt(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a2106013b3082b25589ccf64ade71d837',1,'nvic_generate_software_interrupt(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fget_5factive_5firq',['nvic_get_active_irq',['../nvic_8c.html#a385b545f967c36f2a2df8bf56cc48049',1,'nvic_get_active_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a385b545f967c36f2a2df8bf56cc48049',1,'nvic_get_active_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fget_5firq_5fenabled',['nvic_get_irq_enabled',['../nvic_8c.html#a760014a2f28e2c08c2c5ca8995541cf5',1,'nvic_get_irq_enabled(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a760014a2f28e2c08c2c5ca8995541cf5',1,'nvic_get_irq_enabled(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fget_5fpending_5firq',['nvic_get_pending_irq',['../nvic_8c.html#a32e4ae1adc2e9f7f9de43da182004417',1,'nvic_get_pending_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a32e4ae1adc2e9f7f9de43da182004417',1,'nvic_get_pending_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fset_5fpending_5firq',['nvic_set_pending_irq',['../nvic_8c.html#a4fc9f0e63460ac98ddd9e0ba18d611a5',1,'nvic_set_pending_irq(u8 irqn):&#160;nvic.c'],['../nvic_8h.html#a4fc9f0e63460ac98ddd9e0ba18d611a5',1,'nvic_set_pending_irq(u8 irqn):&#160;nvic.c']]],
  ['nvic_5fset_5fpriority',['nvic_set_priority',['../nvic_8c.html#a8d32953be2608382a7adfa053328b7ba',1,'nvic_set_priority(u8 irqn, u8 priority):&#160;nvic.c'],['../nvic_8h.html#a8d32953be2608382a7adfa053328b7ba',1,'nvic_set_priority(u8 irqn, u8 priority):&#160;nvic.c']]]
];
